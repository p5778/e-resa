# E-RESA

E-RESA is a website that offers an online travel booking service. The principle is to offer trips with attractive prices. these reservations work like wonderbox, that is to say that a person can then book a trip and can use this reservation when he wishes during a given period.

## Project context

As part of the WEB programming project, this application was developed with the following technologies :

- Node JS
- Express
- MongoDB :
	- mongoose
- HTTPS 
- SSH certificate
- Passport.js :
	- passport
	- passport-local
	- express-session
- Bcrypt
- Helmet
- Nodemon

## Features

- Login 
- Sign Up 
- Logout
- Update currentUser
- Creat a product
- Update a product
- delete a product
- Get all products
- Add a product to the shopping cart
- Get the shopping cart content
- Delete a product from the shopping cart
- Soon :
	- Get the product (=destination) current weather 
	- Get the product (=destination) forecast weather 

## Project status

Development in progress but version 1.7 available

### Possible evolutions of the project

- Add notes to a product

## How to install E-RESA

```
git clone https://gitlab.com/p5778/e-resa.git
cd ./e-resa/e-resa
npm install
npm start
```
Access the application with the URL : **https://localhost:3000/**

## Dépendances du projet

"bcrypt": "^5.0.1",
"chalk": "^4.1.2",
"connect-flash": "^0.1.1",
"cookie-parser": "~1.4.4",
"debug": "~2.6.9",
"express": "~4.16.1",
"express-session": "^1.17.2",
"fs": "^0.0.1-security",
"helmet": "^5.0.2",
"http-errors": "~1.6.3",
"https": "^1.0.0",
"mongoose": "^6.3.0",
"morgan": "~1.9.1",
"nodemon": "^2.0.15",
"ora": "^5.4.1",
"passport": "^0.5.2",
"passport-local": "^1.0.0",
"pug": "^3.0.2"

## Auteurs du projet

Julien DEWERPE & Fahdy KOUACHE
University : Université Paris Cité
Master degree : Master MIAGE - FA - Valorisation et Protection de la donnée d'Entreprise
Promo : 2021-2023
Professor : Ilyes SGHIR
  
## Diaporama du projet

https://docs.google.com/presentation/d/1d11_bOk60GGQ6cG56BbWXfae2JJa4epsYrF8Hnv-fd4/edit#slide=id.g8bae437e4a_0_27

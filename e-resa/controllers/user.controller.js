const User = require("../models/user.model");
const Product = require("../models/product.model");
const passport = require("passport");

var userController = {
  signup: passport.authenticate("local.signup", {
    successRedirect: "/user/currentUser",
    failureRedirect: "/user/error",
    failureFlash: true,
  }),

  login: passport.authenticate("local.login", {
    successRedirect: "/user/currentUser",
    failureRedirect: "/user/error",
    failureFlash: true,
  }),

  getUser: async function (req, res, next) {
    try {
      let user = await User.findById(req.user);
      res.json(user);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },

  updateUser: async (req, res, next) => {
    try {
      let user = await User.findByIdAndUpdate(req.user, req.body, {
        new: true,
      });
      res.json(user);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },


  error: async function (req, res, next) {
    try {
      var message = req.flash("error");
      res.render('error', {message: message});
    } catch (err) {
      console.error(err);
      next(err);
    }
  },

  logout: function (req, res, next) {
      var idUser = req.user;
      req.logout();
      res.send(idUser.id + ' is logout');  
  },

  getShoppingCartContent: async function(req, res, next){
    try {
      let user = await User.findById(req.user).populate('shoppingCart.product');
      res.json(user.shoppingCart);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },

  addProductToShoppingCart: async function(req, res, next) {
    try {
      let user = await User.findById(req.user).populate('shoppingCart.product');
      user.shoppingCart.push({
        product: await Product.findById(req.params.idProduct)
      });
      await user.save(); //on verif que tous les champs required sont remplis
      res.json(user);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },

  deleteProductInShoppingCart: async function(req, res, next) {
    try {
      let user = await User.findById(req.user).populate('shoppingCart.product');
      //let comment = book.comments.find((el) => el._id == req.params.commentId);
      let productIndex = user.shoppingCart.findIndex(
        (product) => product._id == req.params.productId
      );
      if (productIndex == undefined) {
        res.json({});
      } else {
        user.shoppingCart.splice(productIndex, 1);
        await user.save();
        // res.json(book.shoppingCart[productIndex]);
        res.json(user.shoppingCart);
      }
    } catch (err) {
      console.error(err);
      next(err);
    }
  },


};

module.exports = userController;

const Product = require('../models/product.model');

var productController = {
    //get all products
    getProducts: async function(req, res, next){
        try{
            let products = await Product.find({});
            res.json(products);
        }catch(err){
            console.error(err);
            next(err);
        }
    },

    //get one product
    getProduct: async function(req, res, next){
        try{
            let product = await Product.findById(req.params.idProduct);
            res.json(product);
        }catch(err){
            console.error(err);
            next(err);
        }
    },

    //add one product
    addProduct: async function(req, res, next){
        try{
            let product = await Product.create(req.body);
            res.json(product);
        }catch(err){
            console.error(err);
            next(err);
        }
    },

    //delete one product
    deleteProduct: async function(req, res, next){
        try{
            let product = await Product.findByIdAndDelete(req.params.idProduct);
            res.json(product);
        }catch(err){
            console.error(err);
            next(err);
        }
    },

    //apdate one product with put request
    updateProduct: async function(req, res, next){
        try{
            let product = await Product.findByIdAndUpdate(req.params.idProduct, req.body, {
                new: true
            });
            res.json(product);
        }catch(err){
            console.error(err);
            next(err);
        }
    }
}

module.exports = productController;
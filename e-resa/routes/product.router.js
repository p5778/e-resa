const express = require('express');
var router = express.Router();
const productController = require('../controllers/product.controller');
const auth = require('../middlewares/authenticate.middleware');

router.get('/', auth.isLoggedIn, productController.getProducts);
router.post('/', auth.isLoggedIn, productController.addProduct);
router.get('/:idProduct', auth.isLoggedIn, productController.getProduct);
router.put('/:idProduct', auth.isLoggedIn, productController.updateProduct);
router.delete('/:idProduct', auth.isLoggedIn, productController.deleteProduct);

module.exports = router;
const express = require('express');
var router = express.Router();
const userController = require('../controllers/user.controller');
const auth = require('../middlewares/authenticate.middleware');

router.get('/currentUser', auth.isLoggedIn, userController.getUser);

router.get('/logout', auth.isLoggedIn, userController.logout);

router.post('/signup', auth.notLoggedIn, userController.signup);

router.post('/login', auth.notLoggedIn, userController.login);

router.get('/error', auth.notLoggedIn, userController.error);

router.get('/shoppingCart', auth.isLoggedIn, userController.getShoppingCartContent);

router.post('/shoppingCart/:idProduct', auth.isLoggedIn, userController.addProductToShoppingCart);

router.delete('/shoppingCart/:idProduct', auth.isLoggedIn, userController.deleteProductInShoppingCart);

router.put('/currentUser', auth.isLoggedIn, userController.updateUser);

module.exports = router;
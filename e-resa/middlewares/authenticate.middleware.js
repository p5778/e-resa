var authenticate = {
    isLoggedIn(req, res, next) {
        if (req.isAuthenticated()){
            return next();
        }
        res.send('you have to be logged');
    },
    notLoggedIn(req, res, next) {
        if (!req.isAuthenticated()){
            return next();
        }
        res.send('you have to not be logged');
    }
}

module.exports = authenticate;
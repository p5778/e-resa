const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var productSchema = new Schema(
    {
        title: {type: String, required: true},
        description: {type: String, required: true},
        imgPath: {type: String, required: true},
        price: {type: Number, required: true}
    },
    {
        collection: "products",
        timestamps: true
    }
);

module.exports = mongoose.model('Product', productSchema);

const mongoose = require("mongoose");
const chalk = require("chalk");
const ora = require("ora");
const dbConfig = require("../config/db.config")
const options = {
 
};
 
const url = dbConfig.mongoURI;
mongoose.connect(url, options);
 
var spinner = ora("Trying to connect to the database...\n").start();
var connectionOpened = false;
 
mongoose.connection.on("connecting", function() {
    spinner.start();
});
 
mongoose.connection.on("error", function(error) {
    spinner.stop();
    console.error(chalk.bgKeyword("orange").black(" ERROR  ", chalk.bgKeyword("orange")("Error in MongoDB connection: " + error)));
});
 
mongoose.connection.on("connected", function() {
    spinner.stop();
    console.log(chalk.bgKeyword("green").black("  CONNECTED  "), chalk.green("Connection to the database successfully etablished."));
});
 
mongoose.connection.on("open", function() {
    connectionOpened = true;
});
 
mongoose.connection.on("reconnected", function() {});
 
mongoose.connection.on("disconnected", function() {
    if(connectionOpened) {
        console.log(chalk.bgRed.black(" DISCONNECTED    ", chalk.red("Connection to the database lost.")));
        spinner = ora("Trying to reconnect to the database...\n").start()
    }
});
 
module.exports = mongoose.connection;

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

var userSchema = new Schema({
    username: {type: String, required: true},
    firstname: {type: String, required: false},
    lastname: {type: String, required: false},
    email: {type: String, required: false},
    dateNaissance: {type: Date, required: false},
    adresse: {type: String, required: false},
    mobile: {type: Number, required: false},
    password: {type: String, required: true},
    admin: {type: Number, default: 0},
    shoppingCart: [
        {
            //productId: {type: String, required: true}
            product: {type: mongoose.Schema.Types.ObjectId, ref: 'Product'}
        }
    ]
},
{ collection: 'users', timestamps: true });

userSchema.methods.encryptPassword = function(password) {
    password = password.toString();
    return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
}

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}
module.exports = mongoose.model('User', userSchema);